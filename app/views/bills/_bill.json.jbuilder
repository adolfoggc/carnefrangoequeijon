json.extract! bill, :id, :client, :client_order, :cost, :created_at, :updated_at
json.url bill_url(bill, format: :json)
