# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


	Client.create [
					[name: 'Rochelle Rock', amount: 350],
					[name: 'Julius Rock', amount: 1],
					[name: 'Drew Rock', amount: 10],
					[name: 'Tonya Rock', amount: 7],
					[name: 'Cris Rock', amount: 0]
				]

