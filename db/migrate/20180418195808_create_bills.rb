class CreateBills < ActiveRecord::Migration[5.0]
  def change
    create_table :bills do |t|
      t.string :client
      t.string :client_order
      t.integer :cost

      t.timestamps
    end
  end
end
